
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	<link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet">
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>

    <script src="https://cdn.datatables.net/plug-ins/1.10.15/type-detection/num-html.js"></script>
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <h1>Test!</h1>

    <div class="container">

	    <table id="logs" class="table table-condensed">
	    	<thead>
	    		<tr>
	    			<th>Client</th>
	    			<th>Total</th>
	    			<th>Date</th>
	    			<th class="col-sm-2">Actions</th>
	    		</tr>
	    	</thead>
		  	
		</table>

	   
	</div>

	<script type="text/javascript">
	$(document).ready(function() {
	    
	    oTable = $('#logs').DataTable({
	        "processing": true,
	        "serverSide": true,
	        "ajax": "{{ route('home.getlogs') }}",
     		aoColumns: [
                    {mData: 'user.company.c_name', "defaultContent": "-"},
                    {mData: 'total', "sType": "html-num", "defaultContent": "-"},
                    { mData: 'user.updated_at', "defaultContent": "-" },
                    {mData:'user.company.c_id', render: function(data,type,row,meta) { return '<a type="button" href="home/edit/'+row.user.company.c_id+'" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-pencil"></i>&nbsp;Edit</a><a type="button" href="home/delete/'+row.user.company.c_id+'" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-trash"></i>&nbsp;Delete</a>'
 					}, "defaultContent": "-"},
            ]
	    });

	});
	</script>

  </body>
</html>


