<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class TransferLogsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('transfer_logs')->truncate();
        for ($i=0; $i < 50; $i++) { 

    		DB::table('transfer_logs')->insert([
	            'tl_user_id'=>rand(1, 100),
	            'tl_resource'=>str_random(8)."."."com",
	            'tl_transferred'=> rand(1, 20),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
	        ]);
    	}

    }
}
