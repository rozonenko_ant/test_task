<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
 		DB::table('companies')->truncate();

        for ($i=0; $i < 100; $i++) { 
    		DB::table('companies')->insert([
	            'c_name'=> str_random(10),
            	'c_quota'=> rand(1, 10)." TB",
            	'created_at' => Carbon::now(),
	            'updated_at' => Carbon::now(),
	        ]);
    	}
    
    }
}
