/*-----------------------------------------------------------------------------------*/
/* 		Mian Js Start
/*-----------------------------------------------------------------------------------*/
jQuery(document).ready(function($) {



/**

 * Copyright (c) 2007-2012 Ariel Flesler - aflesler(at)gmail(dot)com | http://flesler.blogspot.com
 * Dual licensed under MIT and GPL.
 * @author Ariel Flesler
 * @version 1.4.3
 */

(function($){var h=$.scrollTo=function(a,b,c){$(window).scrollTo(a,b,c)};h.defaults={axis:'xy',duration:parseFloat($.fn.jquery)>=1.3?0:1,limit:true};h.window=function(a){return $(window)._scrollable()};$.fn._scrollable=function(){return this.map(function(){var a=this,isWin=!a.nodeName||$.inArray(a.nodeName.toLowerCase(),['iframe','#document','html','body'])!=-1;if(!isWin)return a;var b=(a.contentWindow||a).document||a.ownerDocument||a;return/webkit/i.test(navigator.userAgent)||b.compatMode=='BackCompat'?b.body:b.documentElement})};$.fn.scrollTo=function(e,f,g){if(typeof f=='object'){g=f;f=0}if(typeof g=='function')g={onAfter:g};if(e=='max')e=9e9;g=$.extend({},h.defaults,g);f=f||g.duration;g.queue=g.queue&&g.axis.length>1;if(g.queue)f/=2;g.offset=both(g.offset);g.over=both(g.over);return this._scrollable().each(function(){if(!e)return;var d=this,$elem=$(d),targ=e,toff,attr={},win=$elem.is('html,body');switch(typeof targ){case'number':case'string':if(/^([+-]=)?\d+(\.\d+)?(px|%)?$/.test(targ)){targ=both(targ);break}targ=$(targ,this);if(!targ.length)return;case'object':if(targ.is||targ.style)toff=(targ=$(targ)).offset()}$.each(g.axis.split(''),function(i,a){var b=a=='x'?'Left':'Top',pos=b.toLowerCase(),key='scroll'+b,old=d[key],max=h.max(d,a);if(toff){attr[key]=toff[pos]+(win?0:old-$elem.offset()[pos]);if(g.margin){attr[key]-=parseInt(targ.css('margin'+b))||0;attr[key]-=parseInt(targ.css('border'+b+'Width'))||0}attr[key]+=g.offset[pos]||0;if(g.over[pos])attr[key]+=targ[a=='x'?'width':'height']()*g.over[pos]}else{var c=targ[pos];attr[key]=c.slice&&c.slice(-1)=='%'?parseFloat(c)/100*max:c}if(g.limit&&/^\d+$/.test(attr[key]))attr[key]=attr[key]<=0?0:Math.min(attr[key],max);if(!i&&g.queue){if(old!=attr[key])animate(g.onAfterFirst);delete attr[key]}});animate(g.onAfter);function animate(a){$elem.animate(attr,f,g.easing,a&&function(){a.call(this,e,g)})}}).end()};h.max=function(a,b){var c=b=='x'?'Width':'Height',scroll='scroll'+c;if(!$(a).is('html,body'))return a[scroll]-$(a)[c.toLowerCase()]();var d='client'+c,html=a.ownerDocument.documentElement,body=a.ownerDocument.body;return Math.max(html[scroll],body[scroll])-Math.min(html[d],body[d])};function both(a){return typeof a=='object'?a:{top:a,left:a}}})(jQuery);
    /************************************************************************************ TO TOP STARTS */

    $(window).scroll(function() {
        if ($(this).scrollTop() > 100) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });

    $('.scrollup').on("click", function() {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });



"use strict"



	//jQuery(".ownmenu li.menu-item-type-custom").addClass("scroll");
    jQuery('.has-menu-scroll .ownmenu li.menu-item-type-custom').addClass("scroll");
	/*-----------------------------------------------------------------------------------*/
	/*		STICKY NAVIGATION
	/*-----------------------------------------------------------------------------------*/
	jQuery("header .sticky").sticky({topSpacing:0});
	
	/*-----------------------------------------------------------------------------------*/
	/* 	LOADER
	/*-----------------------------------------------------------------------------------*/
	jQuery("#loader").delay(500).fadeOut("slow");
	
	/*-----------------------------------------------------------------------------------*/
	/*  FULL SCREEN
	/*-----------------------------------------------------------------------------------*/
	jQuery('.full-screen').superslides({});

    /*-----------------------------------------------------------------------------------*/
    /* 	TESTIMONIAL SLIDER
     /*-----------------------------------------------------------------------------------*/
    jQuery(".single-slide").owlCarousel({
        items : 1,
        autoplay:true,
        loop:true,
        autoplayTimeout:5000,
        autoplayHoverPause:true,
        singleItem	: true,
        navigation : true,
        navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
        pagination : true,
        animateOut: 'fadeOut'
    });
    jQuery('.item-slide').owlCarousel({
        loop:true,
        margin:30,
        nav:false,
        navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
        responsive:{
            0:{
                items:1
            },
            400:{
                items:2
            },
            900:{
                items:3
            },
            1200:{
                items:4
            }
        }
    });
	

	/*-----------------------------------------------------------------------------------*/
	/* 		Active Menu Item on Page Scroll
	/*-----------------------------------------------------------------------------------*/
	jQuery(window).scroll(function(event) {
		Scroll();
	});
	jQuery(".scroll a").on("click", function() {
		jQuery('html, body').animate({scrollTop: jQuery(this.hash).offset().top -0}, 1000);
			return false;
	});
	// User define function
	function Scroll() {
	var contentTop      =   [];
	var contentBottom   =   [];
	var winTop      =   jQuery(window).scrollTop();
	var rangeTop    =   5;
	var rangeBottom =   1000;
	jQuery('nav').find('.scroll a').each(function(){
		contentTop.push( jQuery( jQuery(this).attr('href') ).offset().top);
		contentBottom.push( jQuery( jQuery(this).attr('href') ).offset().top + jQuery( jQuery(this).attr('href') ).height() );
	})
	jQuery.each( contentTop, function(i){
		if ( winTop > contentTop[i] - rangeTop ){
			jQuery('nav li.scroll')
			  .removeClass('active')
				.eq(i).addClass('active');
		}}
	)};

    /*-----------------------------------------------------------------------------------*/
    /* 	MOBILE MENU DROPDOWN
    /*-----------------------------------------------------------------------------------*/
	$('.ownmenu .menu-item-has-children > a').append('<i class="menu-toggle fa fa-angle-down"></i>');
	
	$(document).on('click', '.ownmenu .menu-toggle', function(e){
		$(this).closest('li').addClass('active').siblings('.active').removeClass('active');
		$(this).closest('li').siblings('.menu-item-has-children').find('ul').slideUp();
		$(this).parent().siblings('ul').slideToggle();
		e.preventDefault();
	});	

});



/*----- cart-plus-minus-button -----*/
jQuery(".pizza-add-sub").append('<div class="plus qty-pizza">+</div><div class="mines qty-pizza">-</div>');
jQuery(".qty-pizza").on("click", function() {
var $button = jQuery(this);
var oldValue = $button.parent().find("input").val();
if ($button.text() == "+") {
  var newVal = parseFloat(oldValue) + 1;
} else {
  // Don't allow decrementing below zero
 if (oldValue > 0) {
	var newVal = parseFloat(oldValue) - 1;
	} else {
	newVal = 0;
  }
  }
$button.parent().find("input").val(newVal);
 });

/*-----------------------------------------------------------------------------------*/
/* 		Parallax
/*-----------------------------------------------------------------------------------*/
jQuery('.images-slider').flexslider({
  animation: "fade",
  controlNav: "thumbnails"
});
/*-----------------------------------------------------------------------------------*/
/* 	GALLERY SLIDER
/*-----------------------------------------------------------------------------------*/
jQuery('.block-slide').owlCarousel({
    loop:true,
    margin:30,
    nav:true,
	navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:4
        }
}});

/*-----------------------------------------------------------------------------------
    Animated progress bars
/*-----------------------------------------------------------------------------------*/
jQuery('.progress-bars').waypoint(function() {
  jQuery('.progress').each(function(){
    jQuery(this).find('.progress-bar').animate({
      width:jQuery(this).attr('data-percent')
     },100);
});},
	{
	offset: '100%',
    triggerOnce: true
});
