<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/
Route::get('/', function () {
    return Redirect::to('/home');
});

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');

Route::get('home/getrows', ['as'=>'home.getlogs','uses'=>'HomeController@getCompaniesLogs']);



Route::get('/home/edit/{id}', 'HomeController@edit');
Route::post('/home/save', 'HomeController@save');
Route::get('/home/delete/{id}', 'HomeController@delete');
