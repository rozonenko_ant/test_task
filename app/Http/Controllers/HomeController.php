<?php

namespace App\Http\Controllers;

//Models:
use DB;
use App\Models\User;
use App\Models\Companies;
use App\Models\TransferLogs;
use Auth;
use Response;
use App\Http\Requests;
use App\Http\Requests\CompaniesRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Datatables;

class HomeController extends Controller
{
    public function index()
    {
        return view('home.index');
    }

    public function getCompaniesLogs(Request $request)
    {
        /*echo "<pre>";
        var_dump($request->all());
        die;*/
        $TransferLogs = new TransferLogs;

        $logs = $TransferLogs->getLogs($request->all());
        
        return Datatables::of($logs)
            ->make(true);

        //return view('home.index', ['logs'=>$logs]);
    }


    public function edit($id)
    {
        if(!$id)
           return Redirect::to('/');
            
        $Companies = new Companies;
        $company = $Companies->getCompany($id);
        
        if(!$company)
            abort(404);

        return view('home.form', ['company'=>$company]);
    }

    public function save(CompaniesRequest $request)
    {
        $CompaniesModel = new Companies;
        $company = $CompaniesModel->getCompany($request->input('c_id'));
        if(!$company)
            abort('404');

        $company->c_name = $request->input('c_name');
        $company->c_quota = $request->input('c_quota');
        $company->save();

        return Redirect::to('/');
    }

    public function  delete($id)
    {
    	$CompaniesModel = new Companies;
    	$CompaniesModel->deleteCompany($id);
    	return Redirect::to('/');
    }

}