<?php

namespace App\Models;

use App\Models\User;
use App\Models\TransferLogs;
use Illuminate\Database\Eloquent\Model;
use DB;

class Companies extends Model
{
    protected $primaryKey = 'c_id';

    public function user()
    {
        return $this->hasMany('App\Models\User', 'company_id', 'c_id');
    }

    public function getCompany($id)
    {
        return self::with('user.logs')->find($id);
    }

    public function getCompanyLogs()
    {
    	return self::with('user.logs')->get();
    }

    /*public function edit($id, $data)
    {

    }*/

    public function deleteCompany($id)
    {
        $company = self::find($id);
        $users = $company->user();

        foreach ($users->get() as $key => $user)
            $user->logs()->delete();

        $company->user()->delete();
        $company->delete();
    }
}
