<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
class TransferLogs extends Model
{
    
	public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'tl_user_id');
    }


    public function getLogs()
    {
    	return self::with('user.company')->select(DB::raw('tl_user_id, SUM(	tl_transferred) as total'))->groupBy('tl_user_id')->get();

        //->paginate(15)    
    }

    public function edit()
    {
    	
    }

    /*
    public function delete()
    {

    }*/

}
