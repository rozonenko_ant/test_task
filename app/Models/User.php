<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\TransferLogs;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function company()
    {
        return $this->hasOne('App\Models\Companies', 'c_id', 'company_id');
    }

    public function logs()
    {
        return $this->hasMany('App\Models\TransferLogs', 'tl_user_id', 'id');
    }

    
    /*
    public function edit($id, $data)
    {

    }*/


}
